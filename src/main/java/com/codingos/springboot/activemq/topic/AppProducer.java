package com.codingos.springboot.activemq.topic;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
/**
 * 主题模式 发布者
 */
public class AppProducer {

	private static final String url = "tcp://localhost:61616";
	private static final String topicName = "topic-test";

	public static void main(String[] args) throws JMSException {
		// 1.常见ConnectionFactory
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(url);
		// 2.创建Connection
		Connection connection = connectionFactory.createConnection();
		// 3.启动连接
		connection.start();
		// 4.创建会话 (1参: 是否在事务中处理, 2参: 使用自动应答模式)
		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		// 5.创建一个目标主题
		Destination destination = session.createTopic(topicName);
		// 6.创建发布者
		MessageProducer producer = session.createProducer(destination);

		for (int i = 0; i < 100; i++) {
			// 7.创建消息
			TextMessage textMessage = session.createTextMessage("Message " + i);
			// 8.发送消息
			producer.send(textMessage);
			System.out.println("发送消息: " + textMessage.getText());
		}
		// 9.关闭连接
		connection.close();
	}
}
