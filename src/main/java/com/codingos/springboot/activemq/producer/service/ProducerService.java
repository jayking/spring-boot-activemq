package com.codingos.springboot.activemq.producer.service;

public interface ProducerService {

	void sendMessage(String message);
}
