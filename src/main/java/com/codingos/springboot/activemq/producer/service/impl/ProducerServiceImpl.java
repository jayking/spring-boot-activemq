package com.codingos.springboot.activemq.producer.service.impl;

import javax.annotation.Resource;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import com.codingos.springboot.activemq.producer.service.ProducerService;

public class ProducerServiceImpl implements ProducerService {
	
	@Autowired
	private JmsTemplate jmsTemplate;
//	@Resource(name="queueDestination")
	@Resource(name="topicDestination")
	private Destination destination;

	@Override
	public void sendMessage(String message) {
		jmsTemplate.send(destination,new MessageCreator() {
			
			@Override
			public Message createMessage(Session session) throws JMSException {
				TextMessage textMessage = session.createTextMessage(message);
				return textMessage;
			}
		});
		System.out.println("发送消息: "+message);
	}

}
